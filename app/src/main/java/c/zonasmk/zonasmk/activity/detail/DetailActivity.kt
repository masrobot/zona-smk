package c.zonasmk.zonasmk.activity.detail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import c.zonasmk.zonasmk.R
import c.zonasmk.zonasmk.model.JurusanList
import kotlinx.android.synthetic.main.activity_detail.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.browse
import org.jetbrains.anko.makeCall
import org.jetbrains.anko.okButton
import org.jetbrains.anko.sdk15.coroutines.onClick

class DetailActivity : AppCompatActivity(), DetailView {

    private var nameSMK: String? = ""
    private var uid: String? = ""
    private lateinit var presenter: DetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        initPresenter()
        onAttachView()
    }

    private fun initPresenter() {
        presenter = DetailPresenter()
    }

    override fun onAttachView() {
        presenter.onAttach(this)
        presenter.initFirebase()

        val intent = intent
        nameSMK = intent.getStringExtra("nameSMK")
        uid = intent.getStringExtra("uid")

        setSupportActionBar(toolbar_detail)
        supportActionBar?.apply {
            title = nameSMK?.capitalize()
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }

        presenter.getSMKData(uid as String)
    }

    override fun onDetachView() {
        presenter.onDetach()
    }

    override fun onDestroy() {
        super.onDestroy()
        onDetachView()
    }

    override fun detailData(address: String?, phone: String?, website: String?) {
        tv_address.text = address?.capitalize()
        btn_phone.onClick {
            when (phone != "-") {
                true -> makeCall(phone.toString())
                false -> {
                    alert("Nomor telepon tidak tersedia") {
                        okButton {  }
                    }.show()
                }
            }
        }
        btn_website.onClick {
            when (website != "-") {
                true -> browse(website.toString())
                false -> {
                    alert("Website tidak tersedia") {
                        okButton {  }
                    }.show()
                }
            }
        }
    }

    override fun jurusanData(jurusanList: JurusanList) {
        tv_item_jurusan.text = jurusanList.jurusan1.capitalize()
        tv_item_jurusan2.text = jurusanList.jurusan2.capitalize()
        tv_item_jurusan3.text = jurusanList.jurusan3.capitalize()
        tv_item_jurusan4.text = jurusanList.jurusan4.capitalize()
        tv_item_jurusan5.text = jurusanList.jurusan5.capitalize()
        tv_item_jurusan6.text = jurusanList.jurusan6.capitalize()
        tv_item_jurusan7.text = jurusanList.jurusan7.capitalize()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}
