package c.zonasmk.zonasmk.activity.detail

import android.util.Log
import c.zonasmk.zonasmk.basecontract.Presenter
import c.zonasmk.zonasmk.model.JurusanList
import c.zonasmk.zonasmk.model.SMKList
import com.google.firebase.firestore.FirebaseFirestore

class DetailPresenter: Presenter<DetailView> {

    private var mView: DetailView? = null
    private lateinit var mDatabase: FirebaseFirestore

    override fun onAttach(view: DetailView) {
        mView = view
    }

    override fun onDetach() {
        mView = null
    }

    fun initFirebase() {
        mDatabase = FirebaseFirestore.getInstance()
    }

    fun getSMKData(uid: String) {
        mDatabase.collection("daftarSMK")
            .document(uid)
            .addSnapshotListener { docSnap, e ->
                if (e != null) Log.w("errorDb", "Listen failed.", e)
                val dataSMK = docSnap?.toObject(SMKList::class.java)
                mView?.detailData(
                    dataSMK?.alamat,
                    dataSMK?.telepon,
                    dataSMK?.website
                )

                mView?.jurusanData(dataSMK?.jurusan as JurusanList)
            }
    }
}