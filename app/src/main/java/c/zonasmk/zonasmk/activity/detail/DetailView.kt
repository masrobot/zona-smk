package c.zonasmk.zonasmk.activity.detail

import c.zonasmk.zonasmk.basecontract.View
import c.zonasmk.zonasmk.model.JurusanList

interface DetailView: View {
    fun detailData(address: String?, phone: String?, website: String?)
    fun jurusanData(jurusanList: JurusanList)
}