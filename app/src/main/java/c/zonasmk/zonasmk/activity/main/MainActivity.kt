package c.zonasmk.zonasmk.activity.main

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import c.zonasmk.zonasmk.R
import c.zonasmk.zonasmk.activity.detail.DetailActivity
import c.zonasmk.zonasmk.model.SMKList
import c.zonasmk.zonasmk.utils.displayLocationSettingRequest
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk15.coroutines.onClick

class MainActivity : AppCompatActivity(), MainView {

    private lateinit var presenter: MainPresenter
    private var gpsLatitude: Double? = 0.0
    private var gpsLongtitude: Double? = 0.0
    private var smkData: MutableList<SMKList> = mutableListOf()
    private lateinit var locationManager: LocationManager
    private var hasGps = false
    private var hasNetwork = false
    private var locationGps: Location? = null
    private var locationNetwork: Location? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initPresenter()
        onAttachView()
        mv_main.onCreate(savedInstanceState)
    }

    private fun initPresenter() {
        presenter = MainPresenter()
    }

    @SuppressLint("InflateParams")
    override fun onAttachView() {
        presenter.onAttach(this)
        presenter.initFirebase()
        presenter.getSMKData()

        val mWindow = window
        mWindow.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

        displayLocationSettingRequest(this)
        getGPSCoordinate()

        mv_main.getMapAsync {
            it.mapType = GoogleMap.MAP_TYPE_NORMAL

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager
                    .PERMISSION_GRANTED) {
                it.isMyLocationEnabled = true
            } else {
                requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, 101)
            }

            it.isMyLocationEnabled = true
            it.uiSettings.isCompassEnabled = true
            it.uiSettings.isZoomControlsEnabled = true

            it.run {
                animateCamera(
                    CameraUpdateFactory.newCameraPosition(
                        CameraPosition.Builder().target(LatLng(gpsLatitude as Double, gpsLongtitude as Double))
                            .zoom(13f).build()
                    )
                )

                addCircle(
                    CircleOptions()
                        .center(LatLng(gpsLatitude as Double, gpsLongtitude as Double))
                        .radius(6000.0)
                        .strokeWidth(0f)
                        .fillColor(Color.argb(32, 255, 0, 0))
                )

                addCircle(
                    CircleOptions()
                        .center(LatLng(gpsLatitude as Double, gpsLongtitude as Double))
                        .radius(4000.0)
                        .strokeWidth(0f)
                        .fillColor(Color.argb(32, 255, 255, 0))
                )

                addCircle(
                    CircleOptions()
                        .center(LatLng(gpsLatitude as Double, gpsLongtitude as Double))
                        .radius(2000.0)
                        .strokeWidth(0f)
                        .fillColor(Color.argb(32, 0, 255, 0))
                )
            }

            smkData.forEach { i ->
                it.run {
                    addMarker(
                        MarkerOptions()
                            .position(
                                LatLng(i.koordinat.latitude, i.koordinat.longitude)
                            )
                            .title(i.nama)
                            .snippet(i.alamat)
                    ).tag = i.uid

                    setOnMarkerClickListener {marker ->
                        marker.showInfoWindow()
                        false
                    }

                    setOnInfoWindowClickListener {marker ->
                        val uid = marker.tag
                        val nameSMK = marker.title

                        startActivity<DetailActivity>(
                            "nameSMK" to nameSMK,
                            "uid" to "$uid"
                        )
                    }
                }
            }
        }

        btn_keterangan_zona.onClick {
            keteranganZona()
        }

        btn_about.onClick {
            alert {
                title = getString(R.string.about_zona_smk)
                message = getString(R.string.desc_about_zona_smk)
                isCancelable = false
                okButton {  }
            }.show()
        }
    }

    private fun keteranganZona() {
        alert {
            customView {
                verticalLayout {
                    padding = dip(16)
                    textView {
                        text = context.getString(R.string.keterangan_lingkaran_hijau)
                        textColor = ResourcesCompat.getColor(resources, R.color.zoneGreen, null)

                    }.lparams {
                        bottomMargin = dip(8)
                    }
                    textView {
                        text = context.getString(R.string.keterangan_lingkaran_kuning)
                        textColor = ResourcesCompat.getColor(resources, R.color.zoneYellow, null)
                    }.lparams {
                        bottomMargin = dip(8)
                    }
                    textView {
                        text = context.getString(R.string.keterangan_lingkaran_merah)
                        textColor = ResourcesCompat.getColor(resources, R.color.zoneRed, null)
                    }.lparams {
                        bottomMargin = dip(8)
                        backgroundColor = ResourcesCompat.getColor(resources, android.R.color.black, null)
                    }
                }
            }
        }.show()
    }

    override fun onDetachView() {
        presenter.onDetach()
    }

    override fun onResume() {
        super.onResume()
        mv_main.onResume()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mv_main.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        mv_main.onDestroy()
        onDetachView()
    }

    override fun getData(smkList: List<SMKList>) {
        smkData.clear()
        smkData.addAll(smkList)
    }

    private fun getGPSCoordinate() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
            PackageManager.PERMISSION_GRANTED
        ) {
            locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            hasGps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
            hasNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)

            if (hasGps || hasNetwork) {
                if (hasGps) {
                    locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER, 0, 0f, object : LocationListener {
                            override fun onLocationChanged(location: Location?) {
                                if (location != null) {
                                    locationGps = location
                                    gpsLatitude = locationGps?.latitude
                                    gpsLongtitude = locationGps?.longitude
                                }
                            }

                            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}

                            override fun onProviderEnabled(provider: String?) {}

                            override fun onProviderDisabled(provider: String?) {}
                        })

                    val localGpsLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                    if (localGpsLocation != null) locationGps = localGpsLocation
                } else {
                    startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                }
                if (hasNetwork) {
                    locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, 0, 0f, object : LocationListener {
                            override fun onLocationChanged(location: Location?) {
                                if (location != null) {
                                    locationNetwork = location
                                    gpsLatitude = locationNetwork?.latitude
                                    gpsLongtitude = locationNetwork?.longitude
                                }
                            }

                            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}

                            override fun onProviderEnabled(provider: String?) {}

                            override fun onProviderDisabled(provider: String?) {}
                        })

                    val localNetworkLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                    if (localNetworkLocation != null) locationNetwork = localNetworkLocation
                } else {
                    startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                }

                if (locationGps != null && locationNetwork != null) {
                    if (locationGps?.accuracy as Float > locationNetwork?.accuracy as Float) {
                        gpsLatitude = locationNetwork?.latitude
                        gpsLongtitude = locationNetwork?.longitude
                    } else {
                        gpsLatitude = locationNetwork?.latitude
                        gpsLongtitude = locationNetwork?.longitude
                    }
                }
            } else {
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }
        } else {
            requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, 101)
        }
    }

    private fun requestPermission(permissionType: String, requestCode: Int) {
        ActivityCompat.requestPermissions(this, arrayOf(permissionType), requestCode)
    }

}
