package c.zonasmk.zonasmk.activity.main

import android.util.Log
import c.zonasmk.zonasmk.basecontract.Presenter
import c.zonasmk.zonasmk.model.SMKList
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.QuerySnapshot

class MainPresenter: Presenter<MainView> {

    private var mView: MainView? = null
    private lateinit var mDatabase: FirebaseFirestore

    override fun onAttach(view: MainView) {
        mView = view
    }

    override fun onDetach() {
        mView = null
    }

    fun initFirebase() {
        mDatabase = FirebaseFirestore.getInstance()
    }

    fun getSMKData() {
        mDatabase.collection("daftarSMK")
            .addSnapshotListener { snapshot: QuerySnapshot?, e: FirebaseFirestoreException? ->
                if (e != null) Log.w("ErrorSMK", "Listen failed.", e)
                val dataSMK = snapshot?.toObjects(SMKList::class.java)
                mView?.getData(dataSMK as List<SMKList>)
            }
    }
}