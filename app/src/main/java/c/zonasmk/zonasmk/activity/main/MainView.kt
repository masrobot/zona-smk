package c.zonasmk.zonasmk.activity.main

import c.zonasmk.zonasmk.basecontract.View
import c.zonasmk.zonasmk.model.SMKList

interface MainView: View {
    fun getData(smkList: List<SMKList>)
}