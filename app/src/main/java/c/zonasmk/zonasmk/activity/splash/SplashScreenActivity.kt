package c.zonasmk.zonasmk.activity.splash

import android.Manifest
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import c.zonasmk.zonasmk.R
import c.zonasmk.zonasmk.activity.main.MainActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.jetbrains.anko.startActivity

class SplashScreenActivity : AppCompatActivity() {

    private val permissionsRequired = arrayOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.CALL_PHONE
    )
    private val permissionCallbackConstant = 100
    private lateinit var permissionStatus: SharedPreferences
    private var sentToSettings = false
    private var allGranted = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        permissionStatus = this.getSharedPreferences(getString(R.string.permission_status), Context.MODE_PRIVATE)

        requestPermission()
    }

    private fun nextActivity() {
        GlobalScope.launch {
            delay(2000)
            startActivity<MainActivity>()
            finish()
        }
    }

    private fun requestPermission() {
        if (
            ActivityCompat.checkSelfPermission(this, permissionsRequired[0]) !=
            PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(this, permissionsRequired[1]) !=
            PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(this, permissionsRequired[2]) !=
            PackageManager.PERMISSION_GRANTED
        ) {
            when {
                ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[0])
                        || ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[1])
                        || ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[2])-> {
                    ActivityCompat.requestPermissions(this, permissionsRequired, permissionCallbackConstant)
                }
                permissionStatus.getBoolean(permissionsRequired[0], false) -> {
                    ActivityCompat.requestPermissions(this, permissionsRequired, permissionCallbackConstant)
                }
                else -> ActivityCompat.requestPermissions(this, permissionsRequired, permissionCallbackConstant)
            }

            val editor = permissionStatus.edit()
            editor.putBoolean(permissionsRequired[0], true)
            editor.apply()
        } else {
            nextActivity()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == permissionCallbackConstant) {
            for (i in grantResults.indices) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    allGranted = true
                } else {
                    allGranted = false
                    break
                }
            }
        }

        if (allGranted) {
            nextActivity()
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[0])
            || ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[1])
            || ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[2])) {
            ActivityCompat.requestPermissions(this, permissionsRequired, permissionCallbackConstant)
        }
    }

    override fun onPostResume() {
        super.onPostResume()
        if (sentToSettings) {
            if (ActivityCompat.checkSelfPermission(this, permissionsRequired[0]) ==
                PackageManager.PERMISSION_GRANTED) {
            }
        }
    }

    override fun onBackPressed() {
        // Disable back button
        //super.onBackPressed()
    }

}
