package c.zonasmk.zonasmk.basecontract

interface Presenter<T: View> {
    fun onAttach(view: T)
    fun onDetach()
}