package c.zonasmk.zonasmk.basecontract

interface View {
    fun onAttachView()
    fun onDetachView()
}