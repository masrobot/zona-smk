package c.zonasmk.zonasmk.model

data class JurusanList (
    var jurusan1: String = "",
    var jurusan2: String = "",
    var jurusan3: String = "",
    var jurusan4: String = "",
    var jurusan5: String = "",
    var jurusan6: String = "",
    var jurusan7: String = ""
)