package c.zonasmk.zonasmk.model

import com.google.firebase.firestore.GeoPoint

data class SMKList (
    var uid: String = "",
    var nama: String = "",
    var alamat: String = "",
    var telepon: String = "",
    var website: String = "",
    var jurusan: JurusanList = JurusanList(),
    var koordinat: GeoPoint = GeoPoint(0.0, 0.0)
)