package c.zonasmk.zonasmk.utils

import android.app.Activity
import android.content.Context
import android.content.IntentSender
import android.view.View
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task

// visibility content
fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun displayLocationSettingRequest(context: Context) {
    val mGoogleApiClient = GoogleApiClient.Builder(context)
        .addApi(LocationServices.API)
        .build()
    mGoogleApiClient.connect()

    val locationRequest = LocationRequest.create()
    locationRequest.apply {
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        interval = 10000
        fastestInterval = 10000/2
    }

    val builder = LocationSettingsRequest.Builder()
        .addLocationRequest(locationRequest)
    builder.setAlwaysShow(true)

    val result = LocationServices.getSettingsClient(context)
        .checkLocationSettings(builder.build())
    result.addOnCompleteListener(object : OnCompleteListener<LocationSettingsResponse> {
        override fun onComplete(task: Task<LocationSettingsResponse>) {
            try {
                val response = task.getResult(ApiException::class.java)
            } catch (ex: ApiException) {
                when (ex.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                        val resolvableApiException = ex as ResolvableApiException
                        resolvableApiException.startResolutionForResult(context as Activity, 101)
                    } catch (e: IntentSender.SendIntentException) {

                    }

                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                    }
                }
            }

        }
    })
}